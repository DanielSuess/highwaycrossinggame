package de.danielinberlin.uni.highwaycrossinggame.game.service;


import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class GameUtilTest {

    @Test
    public void checkCollisionToBeFalse() throws Exception {
        // point mass player at 0,0 reaching to 0,0
        // vs point mass vehicle at 1,1 reaching to 1,1
        assertFalse( GameUtil.checkCollisionRectangle( 0, 0, 0, 0, 1, 1, 1, 1 ) );
    }

    @Test
    public void checkCollisionToBeTrue() throws Exception {
        // point mass player at 1,1 reaching to 1,1 like
        // vs point mass vehicle at 1,1 reaching to 1,1
        assertTrue( GameUtil.checkCollisionRectangle( 1, 1, 1, 1, 1, 1, 1, 1 ) );
    }
}