package demo_code;
/**
 * Lesen von Konfigurationsdaten aus einer XML-Datei.
 *
 * @author clecon
 * @version 1.0; 02/2012
 */

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;

// DocumentBuilder, DocumentBuilderFactory,
// ParserConfigurationException

public class KonfigurationXML {

    public void leseKonf() {
        NodeList ndList = null;
        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory
                    .newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document document = builder
                    .parse( Konfiguration.KONFIGURATION_DATEINAME );

            // Lesen von ANZAHL_FIGUREN:
            ndList = document.getElementsByTagName( "ANZAHL_FIGUREN" );
            if ( ndList != null ) {
                Konfiguration.ANZAHL_FIGUREN = new Integer( ndList.item( 0 )
                                                                  .getAttributes().item( 0 ).getNodeValue() ).intValue();
                CustomLogger.log( "+++ Wert von Anzahl Figuren:" );
                CustomLogger.log( ndList.item( 0 ).getAttributes().item( 0 )
                                        .getNodeValue() );
            } // if

            // Lesen von ZUSAMMENSTOSS_ANZEIGEDAUER:
            ndList = document
                    .getElementsByTagName( "ZUSAMMENSTOSS_ANZEIGEDAUER" );
            if ( ndList != null ) {
                Konfiguration.ZUSAMMENSTOSS_ANZEIGEDAUER = new Long( ndList
                                                                             .item( 0 ).getAttributes().item( 0 ).getNodeValue() )
                        .longValue();
                CustomLogger.log( "+++ Wert von Zusammenstoss Anzeigedauer:" );
                CustomLogger.log( ndList.item( 0 ).getAttributes().item( 0 )
                                        .getNodeValue() );
            } // if

            // Lesen von HIGHSCORE_URL:
            ndList = document.getElementsByTagName( "HIGHSCORE_URL" );
            if ( ndList != null ) {
                Konfiguration.HIGHSCORE_URL = ndList.item( 0 ).getAttributes()
                                                    .item( 0 ).getNodeValue();
                CustomLogger.log( "+++ Wert von Highscore-URL:" );
                CustomLogger.log( ndList.item( 0 ).getAttributes().item( 0 )
                                        .getNodeValue() );
            } // if
        } catch ( ParserConfigurationException ex1 ) {
            CustomLogger.error( ex1.getStackTrace() );
        } catch ( IOException ex2 ) {
            System.err.println( "Warnung: Fehler beim Lesen der XML-Datei "
                                        + Konfiguration.KONFIGURATION_DATEINAME
                                        + ", vielleicht existiert sie nicht." );
            CustomLogger.log( "Es werden die Standardwerte verwendet." );
            // CustomLogger.error(ex2.getStackTrace());
        } catch ( SAXException ex3 ) {
            CustomLogger.error( ex3.getStackTrace() );
        } // catch
    } // leseKonf

    /**
     * Testprogramm
     *
     * @param args
     */
    public static void main( String[] args ) {
        KonfigurationXML kxml = new KonfigurationXML();
        kxml.leseKonf();
    } // main
} // class KonfigurationXML
