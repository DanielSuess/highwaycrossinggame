package demo_code;

/**
 * Audio-Klasse (Begleitmusik)
 *
 * @author clecon
 * @version 1.0; 04.02.2012 - 05.02.2012
 */
//import sun.audio.*;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.SourceDataLine;
import javax.sound.sampled.UnsupportedAudioFileException;
import java.io.File;
import java.io.IOException;
import java.util.Date;

public class Audio extends Thread {

    private static final long DAUER_DEFAULT = 28000; // in Millisekunden
    private String dateiname = new String();
    private long dauer = DAUER_DEFAULT; // Dauer der Musik (in Millisekunden)
    private boolean stop = false;
    private boolean running = false;
    private SourceDataLine auline = null;

    // private AudioStream audioStream;

    public Audio( String dateiname ) {
        this.dateiname = dateiname;
    } // Konstruktor I

    public Audio( String filename, long dauer ) {
        this.dateiname = filename;
        this.dauer = dauer;
    } // Konstruktor II

    public void runOld() {
        if ( !Konfiguration.AUDIO_ABSPIELEN ) {
            return;
        } // if

        Date beginnZeit, date;
        long diff;
        // Erneutes Starten unterbinden:
        if ( running ) {
            return;
        } else {
            running = true;
        } // if (else)
        while ( !stop ) {
            beginnZeit = new Date();
            playAudio( dateiname );
            do {
                date = new Date();
                diff = date.getTime() - beginnZeit.getTime();
            } while ( ( diff < dauer ) && ( !stop ) );
            stopAudio();
        } // while
    } // run

    public void stoppen() {
        stop = true;
    } // stoppen

	/*
     * public void stopAudio() { if (!Konfiguration.AUDIO_ABSPIELEN) {
	 * AudioPlayer.player.stop(audioStream); } // if } // stopAudio
	 * 
	 * public void playAudio(String filename) { String soundFile = filename;
	 * CustomLogger.log("+++ Soundfile: \"" + soundFile + "\""); try {
	 * InputStream in = new FileInputStream(soundFile); audioStream = new
	 * AudioStream(in); AudioPlayer.player.start(audioStream); } catch
	 * (Exception e) { CustomLogger.error(e.getStackTrace()); } // catch } // playAudio
	 */

    public void stopAudio() {
        auline.stop();
    }

    public void playAudio( String filename ) {
        dateiname = filename;
    }

    public void run() {
        if ( stop ) {
            return; // Beende Thread wenn gew�nscht
        }
        File soundFile = new File( dateiname );
        if ( !soundFile.exists() ) {
            System.err.println( "Wave file not found: " + dateiname );
            return;
        }

        AudioInputStream audioInputStream = null;
        try {
            audioInputStream = AudioSystem.getAudioInputStream( soundFile );
        } catch ( UnsupportedAudioFileException e1 ) {
            CustomLogger.error( e1.getStackTrace() );
            return;
        } catch ( IOException e1 ) {
            CustomLogger.error( e1.getStackTrace() );
            return;
        }

        AudioFormat format = audioInputStream.getFormat();
        DataLine.Info info = new DataLine.Info( SourceDataLine.class, format );

        try {
            auline = (SourceDataLine) AudioSystem.getLine( info );
            auline.open( format );
        } catch ( LineUnavailableException e ) {
            CustomLogger.error( e.getStackTrace() );
            return;
        } catch ( Exception e ) {
            CustomLogger.error( e.getStackTrace() );
            return;
        }

        auline.start();
        int nBytesRead = 0;
        byte[] abData = new byte[524288];

        try {
            while ( nBytesRead != -1 ) {
                nBytesRead = audioInputStream.read( abData, 0, abData.length );
                if ( nBytesRead >= 0 ) {
                    auline.write( abData, 0, nBytesRead );
                }
            }
        } catch ( IOException e ) {
            CustomLogger.error( e.getStackTrace() );
            return;
        } finally {
            auline.drain();
            auline.close();
        }
    }

    /**
     * Testprogramm
     *
     * @param args
     */
    public static void main( String[] args ) {
        Audio audio = new Audio(
                "sounds/chris liebing - evolution (string theory).wav" );
        audio.run();
    } // main
} // class Audio
