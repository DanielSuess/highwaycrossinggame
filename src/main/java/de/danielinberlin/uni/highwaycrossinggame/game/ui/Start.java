package de.danielinberlin.uni.highwaycrossinggame.game.ui;

import de.danielinberlin.uni.highwaycrossinggame.config.service.ConfigService;
import de.danielinberlin.uni.highwaycrossinggame.game.datamodel.AbstractEntity;
import de.danielinberlin.uni.highwaycrossinggame.game.datamodel.Player;
import de.danielinberlin.uni.highwaycrossinggame.game.service.AudioThread;
import de.danielinberlin.uni.highwaycrossinggame.game.service.GameUtil;
import de.danielinberlin.uni.highwaycrossinggame.game.service.HighwayService;
import de.danielinberlin.uni.highwaycrossinggame.config.service.ImageService;
import de.danielinberlin.uni.highwaycrossinggame.game.service.ScoreService;
import de.danielinberlin.uni.highwaycrossinggame.game.service.TimerService;
import de.danielinberlin.uni.highwaycrossinggame.highscore.service.HighscoreService;
import de.danielinberlin.uni.highwaycrossinggame.highscore.ui.HighscorePanel;
import de.danielinberlin.uni.highwaycrossinggame.logger.CustomLogger;

import javax.management.InstanceNotFoundException;
import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Observable;
import java.util.Observer;

public class Start extends JFrame implements // ActionListener,
        Observer {

    private static final long serialVersionUID = -7371666075711613383L;
    // public class Start extends JFrame implements ActionListener, Observer {

    // shortcuts !?
    private HighwayService myHighwayService = HighwayService.getInstance();
    private TimerService timerService = TimerService.getInstance();

    private boolean isRunning = false;
    private boolean isPaused = false;
    private static Player player;
    private int playerFootStep = 0;
    private static boolean collissionDetected = false;
    private JPanel jp;
    private AudioThread audioThread;

//    interface Lauscher {
//        void aktion();
//    }

//    Map<ConfigService.EREIGNIS, Lauscher> buttonActions = new EnumMap<ConfigService.EREIGNIS, Lauscher>(
//            ConfigService.EREIGNIS.class );
//

    /**
     * loads audioThread, player and vehicles
     */
    private void setupHighway() {
        // erzeuge TimerService als Observable
        CustomLogger.log( "setupHighway gestartet" );

        TimerService.getObservableTicker().addObserver( this ); // wegen repaint()

        myHighwayService.clear(); // clear, e.g. because of restart
        for ( int i = 0; i < 6; i++ ) {
            myHighwayService.createVehicle();
        }
        // create Player Instance
        Player.newInstance( timerService.getObservableTicker(), ConfigService.getScreensizeX() / 2
                - ImageService.IMG_PLAYER_WIDTH / 2, ConfigService.getLaneYOffset()
                                    + ( ConfigService.getNumberOfVehicleLanes() + 1 )
                * ConfigService.getLaneHeight(), 7, 1 );
        try {
            player = Player.getInstance();
        } catch ( InstanceNotFoundException e ) {
            CustomLogger.error( e );
        }
    }

    public Start() {
        setupHighway();
        CustomLogger.log( "init()" );

        setTitle( "Highway Crossing Game" );
        setVisible( true );
        setSize( ConfigService.getScreensizeX(), ConfigService.getScreensizeY() );

        jp = (JPanel) getContentPane();
        jp.setLayout( new BoxLayout( jp, BoxLayout.PAGE_AXIS ) );
        jp.setVisible( true );

        MyDrawingPanel jpDraw = MyDrawingPanel.getInstance();
        // jpDraw.setBounds(20, 0, ConfigService.SCREENSIZE_X, ConfigService.SCREENSIZE_Y);
        jp.add( jpDraw );

        // Event Listener
        // btnStart.addActionListener(this);
        setUpKeyActions();
        setUpButtonActions();

        // close listener
        this.addWindowListener( new WindowAdapter() {
            // instead implementing all 7 methods, use an adapter
            @Override
            public void windowClosed( WindowEvent e ) {
                System.exit( 0 );
            }
        } );
    }

    private void setUpKeyActions() {
        this.addKeyListener( new KeyAdapter() {
            @Override
            public void keyPressed( KeyEvent e ) {
                if ( isRunning ) {
                    if ( e.getKeyCode() == KeyEvent.VK_UP ) {
                        changePlayerFeet();
                        player.setY( player.getY()
                                             - ConfigService.getPlayerPixelSpeed() );
                    }
                    if ( e.getKeyCode() == KeyEvent.VK_DOWN ) {
                        changePlayerFeet();
                        player.setY( player.getY()
                                             + ConfigService.getPlayerPixelSpeed() );
                    }
                    if ( e.getKeyCode() == KeyEvent.VK_LEFT ) {
                        changePlayerFeet();
                        player.setX( player.getX()
                                             - ConfigService.getPlayerPixelSpeed() );
                    }
                    if ( e.getKeyCode() == KeyEvent.VK_RIGHT ) {
                        changePlayerFeet();
                        player.setX( player.getX()
                                             + ConfigService.getPlayerPixelSpeed() );
                    }
                    if ( e.getKeyCode() == KeyEvent.VK_ENTER ) {
                        startGame();
                    }
                    if ( e.getKeyCode() == KeyEvent.VK_N ) {
                        CustomLogger.log( "Neustart" );
                        restartGame();
                    }
                    if ( e.getKeyCode() == KeyEvent.VK_P ) {
                        pauseGame();
                    }
                    if ( e.getKeyCode() == KeyEvent.VK_S ) {
                        if ( audioThread.isAlive() ) {
                            audioThread.stopAudio();
                        } else {
                            setupAndStartBackgroundSound();
                        }
                    }
                    if ( e.getKeyCode() == KeyEvent.VK_ESCAPE ) {
                        pauseGame();
                    }
                    // CustomLogger.log("adapter pressed action:" +c);
                }
            }
        } );
    }

    private void setUpButtonActions() {
        // dirty hack for direct button access, fix to map<enum,lauscher> later
        // on
        GameMenuPanel.getbStart().addActionListener( new ActionListener() {
            @Override
            public void actionPerformed( ActionEvent e ) {
                if ( !isRunning ) {
                    startGame();
                } else {
                    pauseGame();
                }
            }
        } );
        GameMenuPanel.getbRestart().addActionListener( new ActionListener() {
            @Override
            public void actionPerformed( ActionEvent e ) {
                restartGame();
            }
        } );
        GameMenuPanel.getcbDebugmode().addChangeListener( new ChangeListener() {
            @Override
            public void stateChanged( ChangeEvent arg0 ) {
                boolean b = ( (JCheckBox) arg0.getSource() ).isSelected();
                CustomLogger.log( "Debugmode=" + b );
                ConfigService.setDebugmode( b );
            }
        } );
        GameMenuPanel.bExit().addActionListener( new ActionListener() {
            @Override
            public void actionPerformed( ActionEvent e ) {
                System.exit( 0 );
            }
        } );
        GameMenuPanel.bShowHighscore().addActionListener( new ActionListener() {
            @Override
            public void actionPerformed( ActionEvent e ) {
                HighscoreService.getHighScoreFromXml();
                HighscoreService.exportXMLFile();
            }
        } );
        // TODO implement map for alle gamepanel buttons
        /*
        buttonActions.put(ConfigService.EREIGNIS.START, new Lauscher() {
			@Override
			public void aktion() {
				startGame();
			}
		});
		buttonActions.put(ConfigService.EREIGNIS.RESTART, new Lauscher() {
			@Override
			public void aktion() {
				restartGame();
			}
		});
		buttonActions.put(ConfigService.EREIGNIS.HIGHSCORE, new Lauscher() {
			@Override
			public void aktion() {
				// TODO
			}
		});
		buttonActions.put(ConfigService.EREIGNIS.LOADCONFIG, new Lauscher() {
			@Override
			public void aktion() {
				// TODO
			}
		});
		buttonActions.put(ConfigService.EREIGNIS.SAVECONFIG, new Lauscher() {
			@Override
			public void aktion() {
				// TODO
			}
		});
		buttonActions.put(ConfigService.EREIGNIS.DEBUGMODE, new Lauscher() {
			@Override
			public void aktion() {
				// TODO get Checkbox value
				ConfigService.setDebugmode(true);
			}
		});
		buttonActions.put(ConfigService.EREIGNIS.EXIT, new Lauscher() {
			@Override
			public void aktion() {
			}
		});*/
    }

    /**
     * defines which player image should be displayed
     */
    public void changePlayerFeet() {
        playerFootStep++;
        if ( playerFootStep >= ImageService.IMG_PLAYER.length ) {
            playerFootStep = 0;
        }
        MyDrawingPanel.getInstance().setPlayerFootStep( playerFootStep );
    }

    public void setupAndStartBackgroundSound() {
        audioThread = new AudioThread( ConfigService.getSoundfileMusic() );
        audioThread.start();
    }

    @Override
    public void update( Observable arg0, Object arg1 ) {
        // check all vehicles, if not visible delete it and create new one
        // Iterator klappt nicht, da Elemente nur Kopien sind, keine Referenz
        int vehicleArraySize = myHighwayService.getEntityList().size();
        AbstractEntity eTemp = null;
        for ( int i = 0; i < vehicleArraySize; i++ ) {
            eTemp = myHighwayService.getEntityList().get( i );
            if ( eTemp.getX() > ConfigService.getScreensizeX() ) {
                myHighwayService.createVehicle();
                myHighwayService.removeEntity( i );
            }
        }
        // update score
        // faster lanes give increase multiplier, bottom lane give zero points
        ScoreService
                .updateScore( (int) ( ConfigService.getScreensizeY() / ( player.getY() + ConfigService
                        .getLaneHeight() ) ) );
        ScorePanel.setScoreLabelText( "" + ScoreService.getScore() );

        // check collission
        collissionDetected = GameUtil.checkCollision( player );
        if ( collissionDetected ) {
            CustomLogger.log( "Ouch" );
            // 2 event soundfiles + background music
            new AudioThread( ConfigService.getSoundfileCrash() ).start();
            shutdownGame();
        }
        // check if Player is reaches final lane
        if ( player.getY() + ImageService.IMG_PLAYER_HEIGHT <= ConfigService.getLaneHeight() ) {
            shutdownGame();
            HighscorePanel.showAddHighscoreMenu();
        }
        repaint();
    }

    /**
     * pauses all Movements
     */
    public void pauseGame() {
        if ( !isPaused ) {
            timerService.pauseTimer();
            audioThread.stopAudio();
            ScoreService.setPaused( true );
            MyDrawingPanel.getInstance().setGameMenuVisible( true );
            isPaused = true;
        } else {
            timerService.continueTimer();
            ScoreService.setPaused( false );
            setupAndStartBackgroundSound();
            MyDrawingPanel.getInstance().setGameMenuVisible( false );
            isPaused = false;
        }

        // important after button press, focus back to applet
        requestFocus();
    }

    /**
     * starts all necessary threads
     */
    public void startGame() {
        // setupHighway();
        timerService.startTimer( ConfigService.getRefreshFrequency() );
        setupAndStartBackgroundSound();
        ScoreService.initScore();
        isRunning = true;
        MyDrawingPanel.getInstance().setGameMenuVisible( false );
        // Focus important, otherwise buttons stays focused, and no
        // keyboard input is possible

        // important after button press, focus back to applet
        requestFocus();
    }

    /**
     * if game is running, it shutsdown, re-inits and starts the game
     */
    public void restartGame() {
        if ( isRunning ) {
            shutdownGame();
            setupHighway(); // important
            startGame();
        }
    }

    /**
     * stops all related threads
     */
    public void shutdownGame() {
        audioThread.stopAudio();
        timerService.requestTimerStop();
    }

    public static Player getPlayer() {
        return player;
    }

    public static boolean isCollissionDetected() {
        return collissionDetected;
    }

    public static void main( String[] args ) {
        final Start startInstance = new Start();
        startInstance.setVisible( true );
    }
}
