package de.danielinberlin.uni.highwaycrossinggame.game.datamodel;

public enum FIGURE_TYPE {
    BIKE,
    CAR,
    TRUCK,
    PLAYER
}
