package de.danielinberlin.uni.highwaycrossinggame.game.service;

public class ScoreService {

    // score = long, because it depends on timeInMillis and dont want to cast with every scoreupdate
    // only cast for "addHighscore"
    private static long score = 0;
    private static long lastTime = 0;
    private static boolean isPaused = false;
    private static int delay = 250;

    /**
     * increase score by last time difference multiplied by 1
     */
    public static void updateScore() {
        updateScore( 1 );
    }

    /**
     * increase score by last time difference multiplied by a factor
     *
     * @param multiplier
     */
    public static void updateScore( int multiplier ) {
        if ( isPaused ) {
            return;
        } else
        //increase by last time difference multiplied by a factor
        {
            score += ( GameUtil.getCurrentTime() / delay - lastTime ) * multiplier;
        }
        lastTime = GameUtil.getCurrentTime() / delay;
    }

    /**
     * set score to zero
     */
    public static void initScore() {
        initScore( 0 );
    }

    /**
     * set own score
     *
     * @param startScore
     */
    public static void initScore( int startScore ) {
        lastTime = GameUtil.getCurrentTime() / delay;
        score = startScore;
    }

    /**
     * returns a score, score is a multiple of 5 (5, 10, 15...)
     *
     * @return score
     */
    public static long getScore() {
        return score - ( score % 5 );
    }

    /**
     * for pausing / unpausing the score-counter
     *
     * @param pauseState
     */
    public static void setPaused( boolean pauseState ) {
        isPaused = pauseState;
    }
}
