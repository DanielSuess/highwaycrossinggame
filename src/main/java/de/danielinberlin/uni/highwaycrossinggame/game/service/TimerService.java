package de.danielinberlin.uni.highwaycrossinggame.game.service;

import de.danielinberlin.uni.highwaycrossinggame.config.service.ConfigService;
import de.danielinberlin.uni.highwaycrossinggame.game.datamodel.ObservableTicker;
import de.danielinberlin.uni.highwaycrossinggame.logger.CustomLogger;

import java.io.Serializable;

/**
 * gibt in einem bestimmten Rythmus ein Timersignal
 *
 * @author Daniel Suess
 * @version v x.x
 * @date 01.04.2012
 */
public class TimerService implements Serializable {

    private static final long serialVersionUID = -1727683524803737755L;
    private static final ObservableTicker observableTicker = new ObservableTicker();
    private static TimerService timerServiceInstance = null; // Singleton
    private int counter = 0;
    private MyInnerThread mIT = null;
    private int sleepValue = 0;
    private boolean paused = false;

    private TimerService() {
    } // verstecke Konstrucktor

    public static TimerService getInstance() {
        if ( timerServiceInstance == null ) {
            timerServiceInstance = new TimerService();
        }
        return timerServiceInstance;
    }

    public static ObservableTicker getObservableTicker() {
        return observableTicker;
    }


    //    public void addObserver( Observer o ) {
//        super.addObserver( o );
//    }

    /**
     * starts the timer with 2 refreshs per second
     */
    public void startTimer() {
        startTimer( 2 );
    }

    /**
     * starts the timer with defined sleepValue
     *
     * @param refreshFrequency of refreshs per second
     */
    public void startTimer( int refreshFrequency ) {
        if ( mIT == null ) {
            mIT = new MyInnerThread();
            this.paused = false;
            this.sleepValue = 1000 / refreshFrequency;
            mIT.start(); // vererbte Thread Methode
        }
    }

    /**
     * trigger will be shutted down
     */
    public void requestTimerStop() { // Interrupt via Boolean
        if ( mIT != null )
        // mIT.stopRequested = true;
        {
            mIT.interrupt();
        }
    }

    /**
     * trigger pauses sending notifications
     */
    public void pauseTimer() {
        if ( ConfigService.isDebugModeEnabled() ) {
            CustomLogger.log( "pausing timer" );
        }
        paused = true;
    }

    /**
     * trigger will continue sending notifications
     */
    public void continueTimer() {
        if ( ConfigService.isDebugModeEnabled() ) {
            CustomLogger.log( "unpausing timer" );
        }
        paused = false;
    }

    private class MyInnerThread extends Thread {

        public void run() {
            while ( !isInterrupted() ) {
                try {
                    Thread.sleep( sleepValue );
                    if ( !paused ) {
                        observableTicker.changed( ++counter ); // counter ist hochgezählt
                    }
                } catch ( InterruptedException e ) {
                    System.err.println( "FEHLER bei der Timer-Warteschleife" );
                }
            } // will be killed
            CustomLogger.log( "Timer has been killed" );
            timerServiceInstance = null;
        }
    }
}
