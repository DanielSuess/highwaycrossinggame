package de.danielinberlin.uni.highwaycrossinggame.game.ui;

import javax.swing.*;
import java.awt.*;

// TODO ausbauen, bzgl ActionListener
public class GameMenuPanel {

    static interface Lauscher {

        void aktion();
    }

    private static JPanel jpGameMenuBG = null;
    private static JPanel jpGameMenu = null;
    private static JButton bStart = new JButton( "Start / Weiter" );
    private static JButton bRestart = new JButton( "Neustart" );
    private static JButton bShowHighscore = new JButton( "Highscore Liste" );
    private static JButton bLoadConfig = new JButton( "Lade Config" );
    private static JButton bSaveConfig = new JButton( "Sichere Config" );
    private static JButton bExit = new JButton( "Beenden" );
    private static JCheckBox cbDebugmode = new JCheckBox( "Debugmode" );

    private static void initGameMenuPanel() {
        jpGameMenuBG = new JPanel();
        jpGameMenuBG.setLayout( new BorderLayout() );
        jpGameMenuBG.setPreferredSize( new Dimension( 200, 250 ) );
        jpGameMenu = new JPanel();
        jpGameMenu.setLayout( new GridLayout( 0, 1, 0, 5 ) ); // a single row
        // TODO ActionListener f�r Menu
        // don't want to oversize buttons, so glue top + bottom
        // jpGameMenu.add(Box.createHorizontalGlue());
        jpGameMenu.add( bStart );
        jpGameMenu.add( bRestart );
        jpGameMenu.add( bShowHighscore );
        jpGameMenu.add( bLoadConfig );
        jpGameMenu.add( bSaveConfig );
        jpGameMenu.add( bExit );
        jpGameMenu.add( cbDebugmode );

        //bShowHighscore.setEnabled(false);
        bLoadConfig.setEnabled( false );
        bSaveConfig.setEnabled( false );
        //bExit.setEnabled(false);

        // jpGameMenu.add(Box.createHorizontalGlue());
        jpGameMenuBG.add( jpGameMenu, BorderLayout.NORTH );
        JLabel jlabCopyright = new JLabel( "(c) Daniel Suess 2012" );
        jlabCopyright.setForeground( Color.LIGHT_GRAY );
        jpGameMenuBG.add( jlabCopyright, BorderLayout.SOUTH );
    }

    public static JPanel getGameMenuPanel() {
        //todo check if instantiation works by constructor only
        if ( jpGameMenuBG == null ) {
            initGameMenuPanel();
        }
        return jpGameMenuBG;
    }

//    public void setActionListener( JButton jb, ActionEvent actEvent ) {
//        jb.addActionListener( new ActionListener() {
//            @Override
//            public void actionPerformed( ActionEvent e ) {
//            }
//        } );
//    }

    /**
     * dirty hack for direct button access, fix to map<enum,lauscher> later on
     *
     * @return the bStart
     */
    public static JButton getbStart() {
        return bStart;
    }

    /**
     * dirty hack for direct button access, fix to map<enum,lauscher> later on
     *
     * @return the bShowHighscore
     */
    public static JButton bShowHighscore() {
        return bShowHighscore;
    }

    /**
     * dirty hack for direct button access, fix to map<enum,lauscher> later on
     *
     * @return the bExit
     */
    public static JButton bExit() {
        return bExit;
    }

    /**
     * dirty hack for direct button access, fix to map<enum,lauscher> later on
     *
     * @return the bRestart
     */
    public static JButton getbRestart() {
        return bRestart;
    }

    /**
     * dirty hack for direct button access, fix to map<enum,lauscher> later on
     *
     * @return the cbDebugmode
     */
    public static JCheckBox getcbDebugmode() {
        return cbDebugmode;
    }
}
