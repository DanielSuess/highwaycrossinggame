package de.danielinberlin.uni.highwaycrossinggame.game.dataaccess;

import de.danielinberlin.uni.highwaycrossinggame.logger.CustomLogger;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.UnsupportedAudioFileException;
import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * Provides data access at file systeml.
 */
public class FileAccess {

    public static Image getImage( String fileName ) {
        return ( new ImageIcon( FileAccess.class.getResource( fileName ) ) ).getImage();
    }

    public static AudioInputStream getAudioInputStream( String fileName ) throws IOException, UnsupportedAudioFileException {
        File soundFile = new File( FileAccess.class.getResource( fileName ).getPath() );
        if ( !soundFile.exists() ) {
            CustomLogger.error( "Wave file not found: " + fileName );
            throw new FileNotFoundException( "Wave file not found: " + fileName );
        }

        return AudioSystem.getAudioInputStream( soundFile );
    }
}
