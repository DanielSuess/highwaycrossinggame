package de.danielinberlin.uni.highwaycrossinggame.game.datamodel;

import de.danielinberlin.uni.highwaycrossinggame.config.service.ConfigService;
import de.danielinberlin.uni.highwaycrossinggame.config.service.ImageService;

import javax.management.InstanceNotFoundException;
import java.io.Serializable;
import java.util.Observable;

/**
 * Player Entity, only one Instance allowed (Singleton)
 *
 * @author Daniel Suess
 * @version v x.x
 * @date 22.04.2012
 */
public class Player extends AbstractEntity implements Serializable {

    private static final long serialVersionUID = 7842956452000394993L;
    private static Player p;

    private Player( Observable ticker, int startX, int startY, int lane,
                    int speed ) {
        super( ticker, FIGURE_TYPE.PLAYER, startX, startY, lane, speed );
        setImage( ImageService.IMG_PLAYER[0] );
    }

    public static void newInstance( Observable ticker, int startX, int startY,
                                    int lane, int speed ) {
        p = new Player( ticker, startX, startY, lane, speed );

        // CustomLogger.log("new " + p.toString() + "(" + p.getX() + ","
        // + p.getY() + ")");
    }

    public static Player getInstance() throws InstanceNotFoundException {
        if ( p == null ) {
            throw new InstanceNotFoundException();
        } else {
            return p;
        }
    }

    /**
     * controls, that player stays within the screen
     */
    @Override
    public void setX( int x ) {
        if ( x > 0 && x < ConfigService.getScreensizeX() - ImageService.IMG_PLAYER_WIDTH ) {
            super.setX( x );
        }
    }

    /**
     * controls, that player stays within the screen
     */
    @Override
    public void setY( int y ) {
        if ( y > 0 && y < ConfigService.getScreensizeY() - ImageService.IMG_PLAYER_HEIGHT ) {
            super.setY( y );
        }
    }

    @Override
    public void update( Observable arg0, Object arg1 ) {
        // leave empty, because player must not move left-to-right by time
        // optional if it's a river crossing ;-)
    }

}
