package de.danielinberlin.uni.highwaycrossinggame.game.service;

import de.danielinberlin.uni.highwaycrossinggame.config.service.ConfigService;
import de.danielinberlin.uni.highwaycrossinggame.config.service.ImageService;
import de.danielinberlin.uni.highwaycrossinggame.game.datamodel.AbstractEntity;
import de.danielinberlin.uni.highwaycrossinggame.game.datamodel.Player;
import de.danielinberlin.uni.highwaycrossinggame.logger.CustomLogger;

import java.util.Calendar;


public class GameUtil {

    private GameUtil() {
        // explicit hide constructor, due to static methods
    }

    /**
     * @return true if player hits a vehicle
     */
    public static boolean checkCollision( Player player ) {
        de.danielinberlin.uni.highwaycrossinggame.game.service.HighwayService myHighwayService = de.danielinberlin.uni.highwaycrossinggame.game.service
                .HighwayService
                .getInstance();
        int vehicleArraySize = myHighwayService.getEntityList().size();
        AbstractEntity eTemp = null;
        // Pixel Shorcuts for Player and Vehicle
        int pX0 = player.getX();
        int pX1 = pX0 + ImageService.IMG_PLAYER_WIDTH;
        int pY0 = player.getY();
        int pY1 = pY0 + ImageService.IMG_PLAYER_HEIGHT;
        int vX0 = 0;
        int vX1 = 0;
        int vY0 = 0;
        int vY1 = 0;
        for ( int i = 0; i < vehicleArraySize; i++ ) {
            eTemp = myHighwayService.getEntityList().get( i );
            vX0 = eTemp.getX();
            vY0 = eTemp.getY();
            // quit if vehicle is far away from player
            // so check vehicles +/-1 lane
            // = higher performance
            if ( vY0 > pY0 + ConfigService.getLaneHeight()
                    || vY0 < pY0 - ConfigService.getLaneHeight() ) {
                continue;
            }
            if ( ConfigService.isDebugModeEnabled() ) {
                CustomLogger.log( "ColliCheck " + eTemp.toString() );
            }

            switch ( eTemp.getTyp() ) {
                case BIKE:
                    vX1 = vX0 + ImageService.IMG_BIKE_WIDTH;
                    vY1 = vY0 + ImageService.IMG_BIKE_HEIGHT;
                    break;
                case CAR:
                    vX1 = vX0 + ImageService.IMG_CAR_WIDTH;
                    vY1 = vY0 + ImageService.IMG_CAR_HEIGHT;
                    break;
                case TRUCK:
                    vX1 = vX0 + ImageService.IMG_TRUCK_WIDTH;
                    vY1 = vY0 + ImageService.IMG_TRUCK_HEIGHT;
                    break;
                default:
                    // none
            }
            // old school
            /*
            // if (Bike.class.isInstance(eTemp)) { ... }
			if (eTemp.getTyp() == AbstractEntity.TYP_BIKE) {
				vX1 = vX0 + ImageService.IMG_CAR_WIDTH;
				vY1 = vY0 + ImageService.IMG_CAR_HEIGHT;
			}
			if (eTemp.getTyp() == AbstractEntity.TYP_BIKE) {
				vX1 = vX0 + ImageService.IMG_TRUCK_WIDTH;
				vY1 = vY0 + ImageService.IMG_TRUCK_HEIGHT;
			}*/

            if ( GameUtil.checkCollisionRectangle( pX0, pX1, pY0, pY1, vX0, vX1,
                                                   vY0, vY1 ) ) {
                return true;
            }
        }
        return false;
    }


    /**
     * Check for collision. Visible for test use only.
     * P as Player, V as Vehicle.
     * <pre>
     *    Y1 ________
     *      |        |
     *      |        |
     *    Y0|________|
     *      X0       X1
     * </pre>
     *
     * @param pX0
     * @param pX1
     * @param pY0
     * @param pY1
     * @param vX0
     * @param vX1
     * @param vY0
     * @param vY1
     * @return true | false
     */
    public static boolean checkCollisionRectangle( int pX0, int pX1, int pY0, int pY1,
                                                      int vX0, int vX1, int vY0, int vY1 ) {
        // check if (top left?) corner of player is inside vehicle
        // TODO check different vehicle dimensions / corners for overlapping
        if ( pY0 >= vY0 && pY0 <= vY1
                && pX0 >= vX0 && pX0 <= vX1 ) {
            CustomLogger.log( "CRASH: Player " +
                                      "from (" + pX0 + "," + pY0 + ") " +
                                      "to (" + pX1 + "," + pY1 + ")" +
                                      " ------- " +
                                      " Vehicle " +
                                      "from (" + vX0 + "," + vY0 + ") " +
                                      "to (" + vX1 + "," + vY1 + ")" );
            return true;
        }
        return false;
    }

    public static long getCurrentTime() {
        return Calendar.getInstance().getTimeInMillis();
    }
}
