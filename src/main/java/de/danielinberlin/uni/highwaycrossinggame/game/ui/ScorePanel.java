package de.danielinberlin.uni.highwaycrossinggame.game.ui;

import javax.swing.*;
import java.awt.*;

public class ScorePanel {

    private static JPanel panelInstance;
    private static JLabel scoreLabel = new JLabel( "0" );

    /**
     * @return instance of scorePanel
     */
    public static JPanel getInstance() {
        if ( panelInstance == null ) {
            panelInstance = new JPanel();
            panelInstance.add( scoreLabel );
            panelInstance.setBackground( Color.GRAY );
            scoreLabel.setForeground( Color.WHITE );
        }
        return panelInstance;
    }

    /**
     * @return scoreLabel text
     */
    public static String getScoreLabelText() {
        return scoreLabel.getText();
    }

    /**
     * @param punkte for the scoreLabel
     */
    public static void setScoreLabelText( String punkte ) {
        ScorePanel.scoreLabel.setText( "Punkte: " + punkte );
    }
}
