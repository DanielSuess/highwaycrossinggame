package de.danielinberlin.uni.highwaycrossinggame.game.service; /**
 * AudioThread-Klasse (Begleitmusik)
 *
 * @author clecon
 * @version 1.0; 04.02.2012 - 05.02.2012
 */

import de.danielinberlin.uni.highwaycrossinggame.game.dataaccess.FileAccess;
import de.danielinberlin.uni.highwaycrossinggame.logger.CustomLogger;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.SourceDataLine;
import javax.sound.sampled.UnsupportedAudioFileException;
import java.io.IOException;
import java.io.Serializable;

public class AudioThread extends Thread implements Serializable {

    private static final long serialVersionUID = 8706555286070260851L;
    private String dateiname = "";
    private boolean stop = false;

    public AudioThread( String dateiname ) {
        this.dateiname = dateiname;
    } // Konstruktor I

    /**
     * use this method to stop audio player, because thread.stop() is deprecated
     */
    public void stopAudio() {
        stop = true;
    } // stoppen

    // based on
    // http://www.anyexample.com/programming/java/java_play_wav_sound_file.xml
    @Override
    public void run() {
        final AudioInputStream audioInputStream;
        try {
            audioInputStream = FileAccess.getAudioInputStream( dateiname );
        } catch ( IOException | UnsupportedAudioFileException e ) {
            CustomLogger.error( e );
            return;
        }
        AudioFormat format = audioInputStream.getFormat();
        DataLine.Info info = new DataLine.Info( SourceDataLine.class, format );

        SourceDataLine audioLine;
        try {
            audioLine = (SourceDataLine) AudioSystem.getLine( info );
            audioLine.open( format );
            audioLine.start();
        } catch ( Exception e ) {
            CustomLogger.error( e );
            return;
        }

        int nBytesRead = 0;
        byte[] abData = new byte[24288]; // AudioBuffer kleiner als 524288 !?

        try {
            // playback the audio file
            while ( nBytesRead != -1 ) {
                if ( stop ) {
                    return; // Beende Thread wenn gewünscht
                }
                nBytesRead = audioInputStream.read( abData, 0, abData.length );
                if ( nBytesRead >= 0 ) {
                    audioLine.write( abData, 0, nBytesRead );
                }
            }
        } catch ( IOException e ) {
            CustomLogger.error( e );
            return;
        } finally {
            audioLine.drain();
            audioLine.close();
        }
    }

    /**
     * Testprogramm
     */
//    public static void main(String[] args) {
//            AudioThread audio = new AudioThread(
//					"sounds/CityIntersectionAmbience_by_Soundjay_com.wav");
//			audio.run();
//		}
}
