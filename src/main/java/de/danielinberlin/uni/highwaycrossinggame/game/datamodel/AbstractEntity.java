package de.danielinberlin.uni.highwaycrossinggame.game.datamodel;

import java.awt.*;
import java.util.Observable;
import java.util.Observer;

/**
 * abstract EntityClass
 *
 * @author Daniel Suess
 * @version v x.x
 * @date 01.04.2012
 */
public abstract class AbstractEntity implements Observer {

    private int x, y;
    private int lane;
    private int speedMaximum = 0;
    private int speed = 0;
    private int id;
    private FIGURE_TYPE typ;
    private static int idGlobal;
    private Image img;
    protected ObservableTicker ticker;

    public AbstractEntity() {
        // empty constructor for serialization mechanism
    }

    /**
     * @param ticker the observable timer
     * @param typ    type of vehicle
     * @param startX offset
     * @param startY offset
     * @param lane   the number of the track
     * @param speed  velocity
     */
    public AbstractEntity( Observable ticker, FIGURE_TYPE typ, int startX, int startY,
                           int lane, int speed ) {
        ticker.addObserver( this );
        id = idGlobal;
        this.typ = typ;
        idGlobal++; // ID increment
        this.ticker = (ObservableTicker) ticker;
        x = startX;
        y = startY;
        this.speed = speed;
        this.lane = lane;
    }

    /**
     * returns a String: [class] (#[id]id) at lane [lane] e.g.Bike (#5) at lane
     * 3
     */
    @Override
    public String toString() {
        return getClass() + "(#" + id + ") at lane " + lane;
    }

    @Override
    public void update( Observable o, Object arg1 ) {
        // Schrittweite bestimmen
        int counterDistance = (Integer) ( arg1 ) / 1000 + speed;
        x += counterDistance;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public void setX( int x ) {
        this.x = x;
    }

    public void setY( int y ) {
        this.y = y;
    }

    public int getLane() {
        return lane;
    }

    public int getId() {
        return id;
    }

    public int getSpeed() {
        return speed;
    }

    /**
     * currently not active
     *
     * @return
     */
    public int getSpeedMaximum() {
        return speedMaximum;
    }

    /**
     * @return the typ
     */
    public FIGURE_TYPE getTyp() {
        return typ;
    }

    /**
     * @return the img
     */
    public Image getImage() {
        return img;
    }

    /**
     * @param img the img to set
     */
    public void setImage( Image img ) {
        this.img = img;
    }
}
