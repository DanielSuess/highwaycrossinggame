package de.danielinberlin.uni.highwaycrossinggame.game.ui;

import javax.swing.*;

/**
 * a JPanel, which displays the control keys
 *
 * @author Daniel Suess
 * @version v x.x
 * @date 22.05.2012
 */
public class KeyTipsPanel {

    private static JPanel panelInstance;

    /**
     * @return instance of KeyTipsPanel
     */
    public static JPanel getInstance() {
        if ( panelInstance == null ) {
            panelInstance = new JPanel();
            panelInstance
                    .add( new JLabel(
                            "<html>Bewegen: Pfeiltasten &#8592;&#8594;&#8593;&#8595;<br> Pause: P <br> Menü: Esc <br> Sound an/aus: S" ) );
        }
        return panelInstance;
    }
}
