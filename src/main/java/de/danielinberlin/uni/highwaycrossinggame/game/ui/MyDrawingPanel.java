package de.danielinberlin.uni.highwaycrossinggame.game.ui;

import de.danielinberlin.uni.highwaycrossinggame.config.service.ConfigService;
import de.danielinberlin.uni.highwaycrossinggame.game.datamodel.AbstractEntity;
import de.danielinberlin.uni.highwaycrossinggame.game.datamodel.Player;
import de.danielinberlin.uni.highwaycrossinggame.game.service.HighwayService;
import de.danielinberlin.uni.highwaycrossinggame.config.service.ImageService;
import de.danielinberlin.uni.highwaycrossinggame.logger.CustomLogger;

import javax.swing.*;
import java.awt.*;

/**
 * creates a JPanel, which contains the graphical elements (background,
 * vehicles, player) this class is needed because I want to draw on 1 panel only
 * instead on the whole applet window
 *
 * @author Daniel Suess
 * @version v x.x
 * @date 22.04.2012
 */
public class MyDrawingPanel extends JPanel {

    private static final long serialVersionUID = 3622746218582107217L;
    private static MyDrawingPanel drawPanel;

    private Player player = Start.getPlayer();
    private int playerFootStep = 0;
    private JPanel jpGameMenu = null;
    private JPanel jpKeyTips = null;

    private MyDrawingPanel() { // defined as singleton
        super();
        // this.setLayout(null); // set to null, for placing jpanels by
        // coordinates
        JPanel jpScore = ScorePanel.getInstance();
        jpScore.setLocation( 0, 20 );
        this.add( jpScore );

        // add and position the Key Tips Panel
        jpKeyTips = KeyTipsPanel.getInstance();
        jpKeyTips.setLocation( 300, 200 );
        // jpKeyTips.getSize().height);
        this.add( jpKeyTips );

        // add and position the GameMenuPanel
        jpGameMenu = GameMenuPanel.getGameMenuPanel();
        this.add( jpGameMenu );
        CustomLogger.log( jpGameMenu.getLocation().toString() );
    }

    public static MyDrawingPanel getInstance() {
        if ( drawPanel == null ) {
            drawPanel = new MyDrawingPanel();
        }
        return drawPanel;
    }

    public void setGameMenuVisible( boolean visible ) {
        jpGameMenu.setVisible( visible );
        jpKeyTips.setVisible( visible );
    }

    // public void paint(Graphics g) { //�berschreibt JFrame
    @Override
    public void paintComponent( Graphics g ) {
        // super.paint(g); // l�scht alle gezeichneten Objekte
        super.paintComponent( g );

        g.drawImage( ImageService.IMG_BACKGROUND, 0, 0, null );
        for ( AbstractEntity e : HighwayService.getInstance().getEntityList() ) {

            // old school
            /*
            if (Car.class.isInstance(e)) {
				g.drawImage(ImageService.IMG_CAR, e.getX(), e.getY(), null);
			}
			if (Truck.class.isInstance(e)) {
				g.drawImage(ImageService.IMG_TRUCK, e.getX(), e.getY(), null);
			}
			if (Bike.class.isInstance(e)) {
				g.drawImage(ImageService.IMG_BIKE, e.getX(), e.getY(), null);
			}*/

            g.drawImage( e.getImage(), e.getX(), e.getY(), null );
            if ( ConfigService.isDebugModeEnabled() ) {
                g.drawString( e.toString(), e.getX(), e.getY() - 5 );
            }
        }
        g.drawImage( ImageService.IMG_PLAYER[playerFootStep], player.getX(), player
                .getY(), null );
        if ( ConfigService.isDebugModeEnabled() ) {
            g.drawString( player.toString(), player.getX(), player.getY() + -5 );
        }

        if ( Start.isCollissionDetected() ) {
            g.drawImage( ImageService.IMG_COLLISION, player.getX(), player.getY(), null );
            g.setColor( Color.white );
            g.fillOval( player.getX() + 50, player.getY(), 50, 20 );
            g.setColor( Color.black );
            g.drawString( "Ouch", player.getX() + 60, player.getY() + 15 );
        }
    }

    public void setPlayerFootStep( int playerFootStep ) {
        this.playerFootStep = playerFootStep;
    }
    /*public  void setLauscher(final Map<ConfigService., Lauscher> lauscherMap) {
    }*/
}