package de.danielinberlin.uni.highwaycrossinggame.game.service;

import de.danielinberlin.uni.highwaycrossinggame.config.service.ConfigService;
import de.danielinberlin.uni.highwaycrossinggame.config.service.ImageService;
import de.danielinberlin.uni.highwaycrossinggame.game.datamodel.AbstractEntity;
import de.danielinberlin.uni.highwaycrossinggame.game.datamodel.Bike;
import de.danielinberlin.uni.highwaycrossinggame.game.datamodel.Car;
import de.danielinberlin.uni.highwaycrossinggame.game.datamodel.Truck;
import de.danielinberlin.uni.highwaycrossinggame.game.datamodel.FIGURE_TYPE;
import de.danielinberlin.uni.highwaycrossinggame.logger.CustomLogger;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

/**
 * Stellt das HighwayService-Model bereit als Singleton implementiert
 *
 * @author Daniel Suess
 * @version v x.x
 * @date 11.04.2012
 */
public class HighwayService implements Serializable {

    private static final long serialVersionUID = 1569870883458211909L;
    private static HighwayService highwayServiceInstance = null; // Singleton
    private static List<AbstractEntity> entityList = new LinkedList<AbstractEntity>();

    // Singleton, so private Constructor
    private HighwayService() {

    }

    /**
     * @return active HighwayService-Instance
     */
    public static HighwayService getInstance() {
        if ( highwayServiceInstance == null ) {
            highwayServiceInstance = new HighwayService();
        }
        return highwayServiceInstance;
    }

    public List<AbstractEntity> getEntityList() {
        return entityList;
    }

    public void addEntity( AbstractEntity s ) {
        entityList.add( s );
    }

    public void removeEntity( AbstractEntity s ) {
        entityList.remove( s );
    }

    public void removeEntity( int index ) {
        entityList.remove( index );
    }

    public void clear() {
        entityList.clear();
    }

    // TODO createVehicles in HighwayService �bertragen

    /**
     * creates a defined vehicle on the defined lane
     *
     * @param vehicleLane
     */
    public void createVehicle( final int vehicleLane, final FIGURE_TYPE typeOfVehicle ) {
        int lane = vehicleLane;
        // 12px (yOffset einer Lane)
        int yOffset = 12 + 75 * lane;

        // check if lane-start is free
        int vehicleArraySize = highwayServiceInstance.getEntityList().size();
        AbstractEntity eTemp;
        for ( int i = 0; i < vehicleArraySize; i++ ) {
            eTemp = highwayServiceInstance.getEntityList().get( i );
            if ( eTemp.getLane() == lane && eTemp.getX() < 20 ) {
                lane++;
                if ( lane > 6 ) {
                    lane = 1;
                }
                createVehicle( lane, typeOfVehicle );
                return;
            }
        }
        switch ( typeOfVehicle ) {
            case BIKE:
                addBike( lane, yOffset );
                break;
            case TRUCK:
                addTruck( lane, yOffset );
                break;
            case CAR:
                addCar( lane, yOffset );
                break;
            default:
                CustomLogger.error( "Unknown vehicle type: " + typeOfVehicle );
        }
    }

    private void addCar( int lane, int yOffset ) {
        highwayServiceInstance.addEntity(
                new Car( TimerService.getObservableTicker(),
                         -ImageService.IMG_CAR_HEIGHT, yOffset, lane,
                         ConfigService.getNumberOfVehicleLanes() - lane + 1 ) );
    }

    private void addTruck( int lane, int yOffset ) {
        highwayServiceInstance.addEntity(
                new Truck( TimerService.getObservableTicker(),
                           -ImageService.IMG_TRUCK_WIDTH, yOffset, lane,
                           ConfigService.getNumberOfVehicleLanes() - lane + 1 ) );
    }

    private void addBike( int lane, int yOffset ) {
        highwayServiceInstance.addEntity( new Bike( TimerService.getObservableTicker(),
                                             -ImageService.IMG_BIKE_WIDTH, yOffset, lane,
                                             ConfigService.getNumberOfVehicleLanes() - lane + 1 ) );
    }

    /**
     * creates a random vehicle on the defined lane
     *
     * @param lane
     */
    public void createVehicle( int lane ) {
        // 3 Vehicles to choose from
        final int randomNumber = (int) ( Math.random() * 3 );
        switch ( randomNumber ) {
            case 0:
                createVehicle( lane, FIGURE_TYPE.BIKE );
                break;
            case 1:
                createVehicle( lane, FIGURE_TYPE.TRUCK );
                break;
            case 2:
                createVehicle( lane, FIGURE_TYPE.CAR );
                break;
            default:
                CustomLogger.error( "Unexpected random number out of scope [0;2]: " + randomNumber );
        }
    }

    /**
     * creates a random vehicle on random lane 1-6
     */
    public void createVehicle() {
        createVehicle( 1 + (int) ( Math.random() * 5 ) );
    }
}
