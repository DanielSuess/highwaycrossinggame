package de.danielinberlin.uni.highwaycrossinggame.game.datamodel;

import java.util.Observable;

public class ObservableTicker extends Observable {
    public void changed( int counter ) {
        setChanged();
        notifyObservers( counter );
    }
}
