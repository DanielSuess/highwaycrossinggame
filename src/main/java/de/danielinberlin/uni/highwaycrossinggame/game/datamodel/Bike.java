package de.danielinberlin.uni.highwaycrossinggame.game.datamodel;

import de.danielinberlin.uni.highwaycrossinggame.config.service.ImageService;

import java.util.Observable;


public class Bike extends AbstractEntity {

    public Bike( Observable ticker, int startX, int startY, int lane,
                 int speed ) {
        super( ticker, FIGURE_TYPE.BIKE, startX, startY, lane, speed );
        setImage( ImageService.IMG_BIKE );
    }
}
