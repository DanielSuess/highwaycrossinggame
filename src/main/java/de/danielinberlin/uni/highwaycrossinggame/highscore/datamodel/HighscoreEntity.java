package de.danielinberlin.uni.highwaycrossinggame.highscore.datamodel;

import java.util.Comparator;

/**
 * a highscore element contains playername, score and date
 *
 * @author Daniel Suess
 * @version v x.x
 * @date 23.05.2012
 */
public class HighscoreEntity {

    public static final Comparator<HighscoreEntity> ASCENDING_COMPARATOR = new Comparator<HighscoreEntity>() {
        @Override
        public int compare( HighscoreEntity lhs, HighscoreEntity rhs ) {
            if ( lhs.score.equals( rhs.score ) ) {
                return 0;
            } else {
                return ( lhs.score < rhs.score ) ? 1 : -1;
            }
        }
    };

    // for direct access through the upper class
    public String playername;
    public String date;
    public Integer score;

    public HighscoreEntity( String playername, int score, String date ) {
        this.playername = playername;
        this.score = score;
        this.date = date;
    }
}