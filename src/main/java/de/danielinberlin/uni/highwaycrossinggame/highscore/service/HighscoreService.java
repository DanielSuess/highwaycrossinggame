package de.danielinberlin.uni.highwaycrossinggame.highscore.service;

import de.danielinberlin.uni.highwaycrossinggame.config.service.ConfigService;
import de.danielinberlin.uni.highwaycrossinggame.highscore.dataaccess.XmlFileException;
import de.danielinberlin.uni.highwaycrossinggame.highscore.dataaccess.XmlImportExport;
import de.danielinberlin.uni.highwaycrossinggame.highscore.datamodel.HighscoreEntity;
import de.danielinberlin.uni.highwaycrossinggame.logger.CustomLogger;

import javax.swing.*;
import java.util.LinkedList;
import java.util.List;

public class HighscoreService {

    private static LinkedList<HighscoreEntity> mylist = new LinkedList<HighscoreEntity>();

    private HighscoreService() {
        // explicit hide constructor, due to static methods
    }
    //TODO check if list was changed, otherwise dont export

    public static LinkedList<HighscoreEntity> getHighScoreFromXml() {
        mylist.clear(); // because its restricted to 10 entries
        try {
            final List<HighscoreEntity> highscoreXML = XmlImportExport.importHighscoreXML( ConfigService.getXMLFile() );
            for ( HighscoreEntity entity : highscoreXML ) {
                HighscoreService.addHighscore( entity.playername, entity.score, entity.date );
            }
        } catch ( XmlFileException e ) {
            CustomLogger.error( e );
        }
        return mylist;
    }

    /**
     * sorts the list
     */
    private static void sortList() {
        mylist.sort( HighscoreEntity.ASCENDING_COMPARATOR );
    }

    /**
     * adds an highscore entry, uses the current date if there are more than 10
     * entries, the littlest will be removed finally the list will be sortet
     *
     * @param score
     * @param playername
     * @param date
     */
    public static void addHighscore( String playername, int score, String date ) {
        // slot is empty or player was good enough
        if ( mylist.size() < 10 || wasIGoodEnough( score ) ) {
            // minimize list to 10
            if ( mylist.size() >= 10 ) {
                mylist.removeLast();
            }

            mylist.add( new HighscoreEntity( playername, score, date ) );
            // sort the list
            sortList();
        } else {
            JOptionPane.showMessageDialog( null,
                                           "Diesmal hat's nicht gereicht. Versuche �ber "
                                                   + mylist.getLast().score + " Punkte zu erreichen" );
        }
    }

    /**
     * checks, if player score is higher than the lowest entry in highscorelist
     *
     * @param score
     * @return true or false
     */
    public static boolean wasIGoodEnough( int score ) {
        return score >= mylist.getLast().score;
    }

    public static void exportXMLFile() {
        // TODO xml export

    }
}
