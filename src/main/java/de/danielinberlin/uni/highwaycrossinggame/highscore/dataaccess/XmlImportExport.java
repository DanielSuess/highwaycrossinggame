package de.danielinberlin.uni.highwaycrossinggame.highscore.dataaccess;

import de.danielinberlin.uni.highwaycrossinggame.config.service.ConfigService;
import de.danielinberlin.uni.highwaycrossinggame.highscore.datamodel.HighscoreEntity;
import de.danielinberlin.uni.highwaycrossinggame.logger.CustomLogger;
import org.w3c.dom.Document;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class XmlImportExport extends DefaultHandler {

    private String currentQName = "";
    private String name = "";
    private String date = "";
    private int score = 0;
    private List<HighscoreEntity> highscoreEntityList = new ArrayList<>();

    /**
     * loads the config data from the given file
     *
     * @param filename
     * @return true if loading was successful
     */
    public static List<HighscoreEntity> importHighscoreXML( String filename ) throws XmlFileException {
        XmlImportExport xmlImportExport = new XmlImportExport();

        return xmlImportExport.importXML( XmlImportExport.class.getResource( filename ).getFile());
    }

    private List<HighscoreEntity> importXML( String filename ) throws XmlFileException {
        try {
            SAXParserFactory fac = SAXParserFactory.newInstance();
            SAXParser parser = fac.newSAXParser();
            fac.setValidating( true );
            parser.parse( filename, this );

            return highscoreEntityList;
        } catch ( SAXException e ) {
            throw new XmlFileException( "Error while parsing XML with SAX" );

        } catch ( ParserConfigurationException e ) {
            throw new XmlFileException( "Error while parsing XML with parser" );
        } catch ( IOException e ) {
            throw new XmlFileException( "Error while importing XML file" );
        }
    }

    /**
     * Saves list-entries to the given XML file.
     *
     * @param list            list of highscore entries
     * @param pathAndFilename path and file name
     * @return true if export was successful
     */
    public boolean exportKonfigXML( LinkedList<HighscoreEntity> list,
                                    String pathAndFilename ) {
        try {
            String spacer = "\t";
            FileWriter fw = new FileWriter( pathAndFilename );
            fw.write( "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" );
            fw.write( "\n" );
            fw.write( "<highscore>\n" );
            for ( HighscoreEntity e : list ) {
                fw.write( spacer + "<entry>\n" );
                fw.write( spacer + spacer + "<playername>" + e.playername
                                  + "</playername>\n" );
                fw.write( spacer + spacer + "<score>" + e.score + "</score>\n" );
                fw.write( spacer + spacer + "<date>" + e.date + "</date>\n" );
                fw.write( spacer + "</entry>\n" );
            }
            fw.write( "</highscore>" );
            fw.flush();
        } catch ( IOException e ) {
            // TODO Auto-generated catch block
            CustomLogger.error( e );
        }
        return false;
    }

    /**
     * creates an XML-File at default location and saves list-entries
     *
     * @param list
     * @return
     */
    public boolean exportKonfigXML( LinkedList<HighscoreEntity> list ) {
        exportKonfigXML( list, ConfigService.getDirXML() + ConfigService.getXMLFile() );
        return false;
    }

    public void writeXMLDocment( Document doc, String filename ) {
        File file = new File( filename );

        try {

            TransformerFactory transFactory = TransformerFactory.newInstance();
            Transformer transformer = transFactory.newTransformer();

            // Source und Result setzen:
            Source source = new DOMSource( doc );
            Result result = new StreamResult( file );

            // NEU
            // Eigenschaften setzen
            transformer.setOutputProperty( OutputKeys.ENCODING, "iso-8859-1" );
            // transformer.setOutputProperty(OutputKeys.DOCTYPE_PUBLIC,
            // "-//W3C//DTD SVG 1.1//EN");
            transformer
                    .setOutputProperty( OutputKeys.DOCTYPE_SYSTEM, "adressen" );
            transformer.setOutputProperty( OutputKeys.METHOD, "xml" );

            // Dokument in Datei speichern:
            transformer.transform( source, result );
        } catch ( TransformerException e1 ) {
            CustomLogger.error( e1 );
        } // catch
    } // writeXMLDocument

    /**
     * (Siehe API &quot;DefaultHandler&quot;.)
     */
    @Override
    public void characters( char[] ch, int start, int length )
            throws SAXException {
        String str = new String( ch, start, length ).trim();
        // skip empty char-actions
        if ( str.length() <= 0 ) {
            return;
        }
        if ( currentQName.equals( "playername" ) ) {
            name = str;
        }
        if ( currentQName.equals( "score" ) ) {
            score = Integer.parseInt( str );
        }
        if ( currentQName.equals( "date" ) ) {
            date = str;
        }
    } // characters

    /**
     * (Siehe API &quot;DefaultHandler&quot;.)
     */
    @Override
    public void startElement( String uri, String localName, String qName,
                              Attributes attributes ) throws SAXException {
        currentQName = qName;
    }

    /**
     * (Siehe API &quot;DefaultHandler&quot;.)
     */
    @Override
    public void endElement( String uri, String localName, String qName )
            throws SAXException {
        // CustomLogger.log("End Tag = "+qName +", currQname="+currentQName);
        if ( qName.equals( "entry" ) ) {
            highscoreEntityList.add( new HighscoreEntity( name, score, date ) );
        }
    }
}
