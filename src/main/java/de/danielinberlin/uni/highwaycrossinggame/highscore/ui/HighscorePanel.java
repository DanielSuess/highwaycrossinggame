package de.danielinberlin.uni.highwaycrossinggame.highscore.ui;

import de.danielinberlin.uni.highwaycrossinggame.game.service.GameUtil;
import de.danielinberlin.uni.highwaycrossinggame.game.service.ScoreService;
import de.danielinberlin.uni.highwaycrossinggame.highscore.datamodel.HighscoreEntity;
import de.danielinberlin.uni.highwaycrossinggame.highscore.service.HighscoreService;
import de.danielinberlin.uni.highwaycrossinggame.logger.CustomLogger;

import javax.swing.*;
import java.text.SimpleDateFormat;
import java.util.LinkedList;

public class HighscorePanel {

    private static JPanel instance;

    private HighscorePanel() {
    }

    public static JPanel getInstance() {
        if ( instance == null ) {
            instance = new JPanel();
        }
        return instance;
    }

    // static LinkedList<HighscoreEntity> mylist = new
    // LinkedList<HighscoreEntity>();
    public static void showHighscoreList( LinkedList<HighscoreEntity> list ) {
        list.sort( HighscoreEntity.ASCENDING_COMPARATOR );

        // TODO provide highscore screen, instead just logging
        if ( list.isEmpty() ) {
            CustomLogger.log( "Keine Einträge vorhanden" );
        } else {
            for ( HighscoreEntity e : list ) {
                CustomLogger.log( e.playername + " " + e.score + " " + e.date );
            }
        }
    }

    public static void showAddHighscoreMenu() {

        // get playername
        //showInternalMessageDialog
        String playername = JOptionPane.showInputDialog( "Du hast es in die Wie heißt du: " );
        // add new entry
        SimpleDateFormat sdf = new SimpleDateFormat( "dd.MM.yyyy" );
        String date = sdf.format( GameUtil.getCurrentTime() );
        HighscoreService.addHighscore( playername, (int) ScoreService.getScore(), date );
    }
}
