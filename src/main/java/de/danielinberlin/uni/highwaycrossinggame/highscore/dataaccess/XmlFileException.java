package de.danielinberlin.uni.highwaycrossinggame.highscore.dataaccess;

public class XmlFileException extends Exception {

    private static final long serialVersionUID = 2246891940287100081L;

    public XmlFileException( String message ) {
        super( message );
    }
}
