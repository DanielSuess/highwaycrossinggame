package de.danielinberlin.uni.highwaycrossinggame.logger;

public class CustomLogger {

    private CustomLogger() {
        // explicit hide constructor, due to static methods
    }

    public static void log( String msg ) {
        System.out.println( "LOG: " + msg );
    }

    public static void warn( String msg ) {
        System.out.println( "WARN: " + msg );
    }

    public static void error( String msg ) {
        System.err.println( "ERROR: " + msg );
    }

    public static void error( Exception e ) {
        e.printStackTrace();
    }

    public static void debug( String msg ) {
        System.out.println( "DEBUG: " + msg );
    }
}
