package de.danielinberlin.uni.highwaycrossinggame.config.service;

import de.danielinberlin.uni.highwaycrossinggame.game.dataaccess.FileAccess;

import java.awt.*;

/**
 * provides images and often used values
 *
 * @author Daniel Suess
 * @version v x.x
 * @date 01.05.2012
 */
public class ImageService {

    //TODO how to ensure, config.class finished xml loading before final setup!?
    public static final Image IMG_CAR = FileAccess.getImage( ConfigService.getImageCar() );

    public static final Image IMG_TRUCK = FileAccess.getImage( ConfigService.getImageTruck() );
    public static final Image IMG_BIKE = FileAccess.getImage( ConfigService.getImageBike() );
    public static final Image IMG_BACKGROUND = FileAccess.getImage( ConfigService.getImageBackground() );
    public static final Image IMG_COLLISION = FileAccess.getImage( ConfigService.getImageCollission() );
    public static final int IMG_VEHICLE_HEIGHT = 50;
    public static final int IMG_CAR_WIDTH = IMG_CAR.getWidth( null );
    public static final int IMG_CAR_HEIGHT = IMG_CAR.getHeight( null );
    public static final int IMG_TRUCK_WIDTH = IMG_TRUCK.getWidth( null );
    public static final int IMG_TRUCK_HEIGHT = IMG_TRUCK.getHeight( null );
    public static final int IMG_BIKE_WIDTH = IMG_BIKE.getWidth( null );
    public static final int IMG_BIKE_HEIGHT = IMG_BIKE.getHeight( null );
    public static final int IMG_BACKGROUND_WIDTH = IMG_BACKGROUND.getWidth( null );
    public static final int IMG_BACKGROUND_HEIGHT = IMG_BACKGROUND.getHeight( null );
    // Player Images
    private static final Image IMG_PLAYER00 = FileAccess.getImage( ConfigService.getImagePlayer00() );
    public static final int IMG_PLAYER_WIDTH = IMG_PLAYER00.getWidth( null );
    public static final int IMG_PLAYER_HEIGHT = IMG_PLAYER00.getHeight( null );
    private static final Image IMG_PLAYER01 = FileAccess.getImage( ConfigService.getImagePlayer01() );
    private static final Image IMG_PLAYER02 = FileAccess.getImage( ConfigService.getImagePlayer02() );
    public static final Image[] IMG_PLAYER = {IMG_PLAYER01,
                                              IMG_PLAYER02};
}
