package de.danielinberlin.uni.highwaycrossinggame.config.service;

public class ConfigService {

    // TODO: use build script to copy sound and image assets to bin

    // DEFAULTs
    private static final int SCREENSIZE_X = 800;
    private static final int SCREENSIZE_Y = 600;
    private static final int LANE_HEIGHT = 75;
    private static final int LANE_Y_OFFSET = 12;
    private static final int NUMBER_OF_VEHICLE_LANES = 6;
    private static final int REFRESH_FREQUENCY = 30;
    private static final int PLAYER_PIXEL_SPEED = 25;
    // sounds
    private static final String SOUNDS_DIR = "/sounds/";
    private static final String MUSICFILE = SOUNDS_DIR + "CityIntersectionAmbience_by_Soundjay_com.wav";
    private static final String CRASHFILE = SOUNDS_DIR + "CarCrash.wav";
    // xml
    private static final String XML_DIR = "/xml/";
    private static final String XMLFILE = XML_DIR + "highscore.xml";
    // images
    private static final String IMAGES_DIR = "/images/";
    private static final String IMAGES_DEBUG_DIR = "/images/debugimages/";
    private static final String IMAGE_CAR = IMAGES_DIR + "car.jpg";
    private static final String IMAGE_TRUCK = IMAGES_DIR + "truck.jpg";
    private static final String IMAGE_BIKE = IMAGES_DIR + "bike.jpg";
    private static final String IMAGE_PLAYER00 = IMAGES_DIR + "player00.png";
    private static final String IMAGE_PLAYER01 = IMAGES_DIR + "player01.png";
    private static final String IMAGE_PLAYER02 = IMAGES_DIR + "player02.png";
    private static final String IMAGE_COLLISSION = IMAGES_DIR + "collission.png";
    private static final String IMAGE_BACKGROUND = IMAGES_DIR + "background.jpg";
    // privates
    private static boolean debugmode = false;
    //statics
    private static int screensizeX = SCREENSIZE_X;
    private static int screensizeY = SCREENSIZE_Y;
    private static int laneHeight = LANE_HEIGHT;
    private static int laneYOffset = LANE_Y_OFFSET;
    private static int numberOfVehicleLanes = NUMBER_OF_VEHICLE_LANES;
    private static int refreshFrequency = REFRESH_FREQUENCY;
    private static int playerPixelSpeed = PLAYER_PIXEL_SPEED;
    private static String soundfileMusic = MUSICFILE;
    private static String soundfileCrash = CRASHFILE;
    private static String xmlFile = XMLFILE;
    private static String dirSound = SOUNDS_DIR;
    private static String dirXML = XML_DIR;
    private static String dirImages = IMAGES_DIR;
    private static String imageCar = IMAGE_CAR;
    private static String imageTruck = IMAGE_TRUCK;
    private static String imageBike = IMAGE_BIKE;
    private static String imagePlayer00 = IMAGE_PLAYER00;
    private static String imagePlayer01 = IMAGE_PLAYER01;
    private static String imagePlayer02 = IMAGE_PLAYER02;
    private static String imageCollission = IMAGE_COLLISSION;
    private static String imageBackground = IMAGE_BACKGROUND;

    /**
     * @return the debugmode
     */
    public static boolean isDebugModeEnabled() {
        return debugmode;
    }

    /**
     * @param debugmode the debugmode to set
     */
    public static void setDebugmode( boolean debugmode ) {
        // if debugmode changes
        if ( ConfigService.debugmode != debugmode ) {
            ConfigService.debugmode = debugmode;
            if ( dirImages.equals( IMAGES_DIR ) ) {
                dirImages = IMAGES_DEBUG_DIR;
            } else {
                dirImages = IMAGES_DIR;
            }
        }
    }

    /**
     * @return the screensizeX
     */
    public static int getScreensizeX() {
        return screensizeX;
    }

    /**
     * @return the screensizeY
     */
    public static int getScreensizeY() {
        return screensizeY;
    }

    /**
     * @return the laneHeight
     */
    public static int getLaneHeight() {
        return laneHeight;
    }

    /**
     * @return the laneYOffset
     */
    public static int getLaneYOffset() {
        return laneYOffset;
    }

    /**
     * @return the numberOfVehicleLanes
     */
    public static int getNumberOfVehicleLanes() {
        return numberOfVehicleLanes;
    }

    /**
     * @return the refreshFrequency
     */
    public static int getRefreshFrequency() {
        return refreshFrequency;
    }

    /**
     * @return the playerPixelSpeed
     */
    public static int getPlayerPixelSpeed() {
        return playerPixelSpeed;
    }

    /**
     * @return the soundfileMusic
     */
    public static String getSoundfileMusic() {
        return soundfileMusic;
    }

    /**
     * @return the soundfileCrash
     */
    public static String getSoundfileCrash() {
        return soundfileCrash;
    }

    /**
     * @return the dirXML
     */
    public static String getDirXML() {
        return dirXML;
    }

    /**
     * @return the dirXML
     */
    public static String getXMLFile() {
        return xmlFile;
    }

    /**
     * @return the imageCar
     */
    public static String getImageCar() {
        return imageCar;
    }

    /**
     * @return the imageTruck
     */
    public static String getImageTruck() {
        return imageTruck;
    }

    /**
     * @return the imageBike
     */
    public static String getImageBike() {
        return imageBike;
    }

    /**
     * @return the imagePlayer00
     */
    public static String getImagePlayer00() {
        return imagePlayer00;
    }

    /**
     * @return the imagePlayer01
     */
    public static String getImagePlayer01() {
        return imagePlayer01;
    }

    /**
     * @return the imagePlayer02
     */
    public static String getImagePlayer02() {
        return imagePlayer02;
    }

    /**
     * @return the imageCollission
     */
    public static String getImageCollission() {
        return imageCollission;
    }

    /**
     * @return the imageBackground
     */
    public static String getImageBackground() {
        return imageBackground;
    }
}
